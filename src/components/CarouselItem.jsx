import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import theme from '../assets/styles/theme';

import Label from './Label';
import Card from './Card';

const CarouselItemContainer = styled.div`
  padding: 0 ${theme.margin15x} ${theme.margin15x} 0;
`;
CarouselItemContainer.displayName = 'CarouselItemContainer';

const CarouselItemBanner = styled.div`
  min-height: calc(100vh / 3);
`;
CarouselItemBanner.displayName = 'CarouselItemBanner';

// eslint-disable-next-line object-curly-newline
const CarouselItem = ({ offerType, offerTitle, offerDescription, src }) => (
  <CarouselItemContainer className="CarouselItem__container pr-4 pb-4">
    <div className="pb-2">
      <Label
        className="m-0 text-uppercase"
        color={theme.moriahRedColor}
        h6={offerType}
      />
      <Label className="m-0" h3={offerTitle} />
      <Label className="m-0" h6={offerDescription} />
    </div>
    <Card url={src} backgroundSize="cover">
      <CarouselItemBanner className="mt-1" />
    </Card>
  </CarouselItemContainer>
);

CarouselItem.defaultProps = {
  src: 'https://source.unsplash.com/1600x900/?food,restaurant',
};

CarouselItem.propTypes = {
  offerType: PropTypes.string,
  offerTitle: PropTypes.string,
  offerDescription: PropTypes.string,
  src: PropTypes.string,
};

export default CarouselItem;
