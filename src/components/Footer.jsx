import React from 'react';
import styled from 'styled-components';

import theme from '../assets/styles/theme';

import Label from './Label';

const FooterContainer = styled.footer``;
FooterContainer.displayName = 'FooterContainer';

const Footer = () => (
  <>
    <FooterContainer className="px-3 mb-4">
      <hr className="my-2" />
      <Label
        className="d-flex justify-content-center"
        span="Moriah 2020"
        color={theme.bodyTextColor}
        fontFamily={theme.bodyFont}
      />
    </FooterContainer>
  </>
);

export default Footer;
