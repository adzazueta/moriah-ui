import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import theme from '../assets/styles/theme';

const ImageAnchorContainer = styled.a`
  display: block;
  border-radius: ${theme.radius2x};
  box-shadow: ${theme.shadow1px};
  max-width: ${(props) => props.maxwidth};
  max-height: ${(props) => props.maxheight};
`;
ImageAnchorContainer.displayName = 'ImageAnchorContainer';

const LinkButton = styled(Link)`
  display: block;
  border-radius: ${theme.radius2x};
  box-shadow: ${theme.shadow1px};
  max-width: ${(props) => props.maxwidth};
  max-height: ${(props) => props.maxheight};
`;
LinkButton.displayName = 'ImageLinkContainer';

const Image = styled.img`
  border-radius: ${theme.radius2x};
  object-fit: contain;
  width: 100%;
  height: auto;
`;
Image.displayName = 'Image';

const Button = ({
  children,
  className,
  imageContainerClassName,
  imageButton,
  withoutImage,
  src,
  maxWidth,
  maxHeight,
  href,
  to,
  target,
  alt,
  rel,
}) => {
  if (imageButton) {
    return (
      <ImageAnchorContainer
        className={className}
        maxwidth={maxWidth}
        maxheight={maxHeight}
        href={href}
        to={to}
        target={target}
        rel={rel}
      >
        <Image src={src} alt={alt} />
      </ImageAnchorContainer>
    );
  }

  return (
    <LinkButton
      className={className}
      maxwidth={maxWidth}
      maxheight={maxHeight}
      to={to}
      rel={rel}
    >
      <div className="row">
        {children}
        {!withoutImage && (
          <div className={imageContainerClassName}>
            <Image src={src} alt={alt} />
          </div>
        )}
      </div>
    </LinkButton>
  );
};

Button.defaultProps = {
  maxWidth: '182.5px',
  maxHeight: '182.5px',
  target: '_blank',
};

Button.propTypes = {
  children: PropTypes.element,
  className: PropTypes.string,
  imageContainerClassName: PropTypes.string,
  imageButton: PropTypes.bool,
  withoutImage: PropTypes.bool,
  src: PropTypes.string,
  href: PropTypes.string,
  to: PropTypes.string,
  target: PropTypes.string,
  maxWidth: PropTypes.string,
  maxHeight: PropTypes.string,
  alt: PropTypes.string,
  rel: PropTypes.string,
};

export default Button;
