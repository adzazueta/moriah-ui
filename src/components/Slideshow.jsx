import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';

const Slideshow = ({
  children,
  adaptiveHeight,
  arrows,
  centerMode,
  centerPadding,
  className,
  infiniteSlide,
  slidesToScroll,
  slidesToShow,
  speed,
  useRow,
}) => {
  const settings = {
    adaptiveHeight,
    arrows,
    centerMode,
    centerPadding,
    className,
    infiniteSlide,
    slidesToScroll,
    slidesToShow,
    speed,
    useRow,
  };

  if (children) {
    return (
      <Slider {...settings}>
        {children.filter((child) => child !== null)}
      </Slider>
    );
  }

  return null;
};

Slideshow.defaultProps = {
  adaptiveHeight: false,
  arrows: false,
  centerMode: true,
  centerPadding: '16px',
  className: 'center',
  infiniteSlide: true,
  slidesToScroll: 1,
  slidesToShow: 1,
  speed: 300,
  useRow: false,
};

Slideshow.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element),
  adaptiveHeight: PropTypes.bool,
  arrows: PropTypes.bool,
  centerMode: PropTypes.bool,
  centerPadding: PropTypes.string,
  className: PropTypes.string,
  infiniteSlide: PropTypes.bool,
  slidesToScroll: PropTypes.number,
  slidesToShow: PropTypes.number,
  speed: PropTypes.number,
  useRow: PropTypes.bool,
};

export default Slideshow;
