/* eslint-disable jsx-a11y/no-noninteractive-tabindex */
import React from 'react';
import styled from 'styled-components';

import theme from '../assets/styles/theme';

const MapContainer = styled.div`
  height: calc(100vh / 3);
  border-radius: ${theme.radius2x};
  border: none;
  box-shadow: ${theme.shadow1px};
`;
MapContainer.displayName = 'MapContainer';

const Map = () => (
  <MapContainer className="w-100">
    <iframe
      title="moriah map"
      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3732.8756980424737!2d-103.28839708452237!3d20.674635486191363!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8428b1ae48281545%3A0x77b7109c4de2eb2d!2sTerraza%20Moriah!5e0!3m2!1ses!2smx!4v1596676154701!5m2!1ses!2smx"
      width="100%"
      height="100%"
      frameBorder="0"
      style={{ border: 0 }}
      allowFullScreen=""
      aria-hidden="false"
      tabIndex="0"
    />
  </MapContainer>
);

export default Map;
