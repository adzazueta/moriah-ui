import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import theme from '../assets/styles/theme';

import Label from './Label';

const ProductItemContainer = styled.div``;
ProductItemContainer.displayName = 'ProductItemContainer';

const ProductItem = ({ productName, productDescription, productPrizes }) => (
  <ProductItemContainer>
    <div className="row">
      <div className="col-9">
        <Label
          className="mb-0"
          h5={productName}
          fontFamily={theme.bodyFont}
          color={theme.headingTextColor}
        />
        <Label
          className="mb-0 mx-0"
          p={productDescription}
          color={theme.bodyTextColor}
        />
      </div>
      <div className="col-3 d-flex justify-content-end">
        <div>
          {productPrizes.map((item) => (
            <Label
              key={`${productName}${item.size}${item.price}`}
              className="mb-0"
              h5={`${item.size} $${item.price}`}
              fontFamily={theme.bodyFont}
              color={theme.headingTextColor}
            />
          ))}
        </div>
      </div>
    </div>
  </ProductItemContainer>
);

ProductItem.defaultProps = {
  productName: 'Hamburguesa Moriah',
  productDescription: 'Hamburguesa de bagel con carne casera y papas gajo',
  productPrizes: [
    { size: 'M', amount: '70' },
    { size: 'G', amount: '90' },
  ],
};

ProductItem.propTypes = {
  productName: PropTypes.string,
  productDescription: PropTypes.string,
  productPrizes: PropTypes.arrayOf(PropTypes.object),
};

export default ProductItem;
