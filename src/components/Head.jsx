import React from 'react';
import { Helmet } from 'react-helmet';

import Favicon from '../assets/favicon/favicon.ico';
import Favicon16 from '../assets/favicon/favicon-16x16.png';
import Favicon32 from '../assets/favicon/favicon-32x32.png';
import AppleTouchIcon from '../assets/favicon/apple-touch-icon.png';
import SafariPinnedTab from '../assets/favicon/safari-pinned-tab.svg';
import Manifest from '../assets/favicon/site.webmanifest';
import BrowserConfig from '../assets/favicon/browserconfig.xml';

const Head = () => (
  <Helmet>
    <link rel="shortcut icon" href={Favicon} />
    <link rel="icon" type="image/png" sizes="16x16" href={Favicon16} />
    <link rel="icon" type="image/png" sizes="32x32" href={Favicon32} />
    <link rel="apple-touch-icon" sizes="180x180" href={AppleTouchIcon} />
    <link rel="mask-icon" href={SafariPinnedTab} color="#000000" />
    <link rel="manifest" href={Manifest} />
    <meta name="msapplication-config" content={BrowserConfig} />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="theme-color" content="#ffffff" />
  </Helmet>
);

export default Head;
