/* eslint-disable import/prefer-default-export */
import React from 'react';
import PropTypes from 'prop-types';

import theme from '../assets/styles/theme';

export const MoriahLogo = ({ color, size, className }) => (
  <svg
    className={className}
    id="Layer_1"
    width={size}
    height={size}
    viewBox="0 0 385.7 303.5"
    style={{
      enableBackground: 'new 0 0 385.7 303.5',
    }}
    xmlSpace="preserve"
  >
    <style type="text/css">{`.st0{fill:${color};}`}</style>
    <g id="XMLID_65_">
      <path
        id="XMLID_66_"
        className="st0"
        d="M199,93.8c-2.3-1.7-5.1-2.3-7.9-1.9c-2.8,0.5-5.3,2-6.9,4.3c-1.7,2.3-2.3,5.1-1.9,7.9 c0.5,2.8,2,5.3,4.3,6.9c1.9,1.3,4,2,6.2,2c3.3,0,6.6-1.5,8.7-4.4C204.9,103.9,203.8,97.3,199,93.8z"
      />
      <path
        id="XMLID_67_"
        className="st0"
        d="M385.5,99.7c-0.5-2.8-2-5.3-4.3-6.9c-4.8-3.4-11.4-2.3-14.9,2.5c-1.7,2.3-2.3,5.1-1.9,7.9 c0.5,2.8,2,5.3,4.3,6.9c1.8,1.3,4,2,6.2,2c0.6,0,1.2,0,1.8-0.1c2.8-0.5,5.3-2,6.9-4.3C385.4,105.3,386,102.5,385.5,99.7z"
      />
      <path
        id="XMLID_68_"
        className="st0"
        d="M310.2,2c-2.3-1.7-5.1-2.3-7.9-1.9c-2.8,0.5-5.3,2-6.9,4.3c-3.4,4.8-2.3,11.5,2.5,14.9 c1.9,1.3,4,2,6.2,2c3.3,0,6.6-1.5,8.7-4.4C316.1,12.1,315,5.4,310.2,2z"
      />
      <path
        id="XMLID_69_"
        className="st0"
        d="M4.5,92.7c-2.3,1.7-3.8,4.1-4.3,6.9c-0.5,2.8,0.2,5.6,1.9,7.9c1.7,2.3,4.1,3.8,6.9,4.3 c0.6,0.1,1.2,0.1,1.8,0.1c2.2,0,4.4-0.7,6.2-2c4.8-3.4,5.9-10.1,2.5-14.9C15.9,90.4,9.2,89.3,4.5,92.7z"
      />
      <path
        id="XMLID_70_"
        className="st0"
        d="M82.5,0.1c-2.8-0.5-5.6,0.2-7.9,1.9c-2.3,1.7-3.8,4.1-4.3,6.9c-0.5,2.8,0.2,5.6,1.9,7.9 c2.1,2.9,5.4,4.4,8.7,4.4c2.1,0,4.3-0.6,6.2-2c4.8-3.4,5.9-10.1,2.5-14.9C87.8,2.1,85.4,0.6,82.5,0.1z"
      />
    </g>
    <path
      id="XMLID_471_"
      className="st0"
      d="M317.1,57L193.4,230.2c-0.3,0.4-0.9,0.4-1.1,0L68.5,57c-0.1-0.2-0.3-0.3-0.6-0.3H52.5 c-0.4,0-0.7,0.3-0.7,0.7v245.4c0,0.4,0.3,0.7,0.7,0.7h280.7c0.4,0,0.7-0.3,0.7-0.7V57.4c0-0.4-0.3-0.7-0.7-0.7h-15.5 C317.4,56.7,317.2,56.8,317.1,57z M71.3,283.3v-41.2c0-0.7,0.9-0.9,1.3-0.4l30,41.2c0.3,0.5,0,1.1-0.6,1.1H72 C71.6,284,71.3,283.6,71.3,283.3z M127.4,283.7L71.4,207c-0.1-0.1-0.1-0.3-0.1-0.4v-35.2c0-0.7,0.9-1,1.3-0.4l78.8,111.9 c0.3,0.5,0,1.1-0.6,1.1h-22.9C127.7,284,127.5,283.8,127.4,283.7z M71.3,135V96.7c0-0.7,0.9-1,1.3-0.4l108,151.1 c0.2,0.2,0.2,0.6,0,0.8l-14.4,20.1c-0.3,0.4-0.9,0.4-1.1,0L71.4,135.4C71.4,135.3,71.3,135.2,71.3,135z M313.6,284h-30 c-0.6,0-0.9-0.6-0.6-1.1l30-41.2c0.4-0.5,1.3-0.3,1.3,0.4v41.2C314.3,283.6,314,284,313.6,284z M314.2,207l-55.9,76.7 c-0.1,0.2-0.3,0.3-0.6,0.3h-22.9c-0.6,0-0.9-0.6-0.6-1.1L313.1,171c0.4-0.6,1.3-0.3,1.3,0.4v35.2C314.3,206.7,314.3,206.9,314.2,207 z M314.2,135.4L209.8,283.7c-0.1,0.2-0.3,0.3-0.6,0.3h-28.9c-0.6,0-0.9-0.6-0.6-1.1L313.1,96.3c0.4-0.5,1.3-0.3,1.3,0.4V135 C314.3,135.2,314.3,135.3,314.2,135.4z"
    />
  </svg>
);

export const ExpandMoreIcon = ({ color, size, className }) => (
  <svg
    className={className}
    id="Layer_1"
    width={size}
    height={size}
    fill={color}
    viewBox="0 0 24 24"
  >
    <path d="M0 0h24v24H0z" fill="none" />
    <path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z" />
  </svg>
);

MoriahLogo.defaultProps = {
  color: theme.blackColor,
  size: theme.iconSize,
};

MoriahLogo.propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
  className: PropTypes.string,
};

ExpandMoreIcon.defaultProps = {
  color: '#000000',
  size: 24,
};

ExpandMoreIcon.propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
  className: PropTypes.string,
};
