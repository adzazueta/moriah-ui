import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import theme from '../assets/styles/theme';

import { MoriahLogo } from './Icons';
import Label from './Label';

const NavbarContainer = styled.nav`
  background: ${theme.mainColor};
  height: 70px;

  .logo {
    position: relative;
    top: 6px;
  }
`;
NavbarContainer.displayName = 'NavbarContainer';

const Navbar = () => (
  <NavbarContainer className="d-flex align-items-center justify-content-start px-3 mb-4">
    <Link to="/">
      <MoriahLogo className="mr-2" />
      <Label
        className="text-uppercase ml-1 logo"
        span="Moriah"
        color={theme.headingTextColor}
        fontFamily={theme.headingFont}
        fontSize={theme.logoFontSize}
        fontWeight={theme.logoFontWeight}
        letterSpacing={theme.logoLetterSpacing}
      />
    </Link>
  </NavbarContainer>
);

export default Navbar;
