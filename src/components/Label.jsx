import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import theme from '../assets/styles/theme';
import mediaQueries from '../assets/styles/mediaQueries';

const H1Container = styled.h1`
  color: ${(props) => props.color};
  font-family: ${(props) => props.fontFamily};
  font-size: ${(props) => props.fontSize};
  line-height: ${(props) => props.lineHeight};
  font-weight: ${(props) => props.fontWeight};
  letter-spacing: ${(props) => props.letterSpacing};

  @media (min-width: ${mediaQueries.min.large}) {
    font-size: ${(props) => props.desktopFontSize};
    line-height: ${(props) => props.desktopLineHeight};
  }
`;
H1Container.displayName = 'H1Container';

H1Container.defaultProps = {
  color: theme.headingTextColor,
  fontFamily: theme.headingFont,
};

const H2Container = styled.h2`
  color: ${(props) => props.color};
  font-family: ${(props) => props.fontFamily};
  font-size: ${(props) => props.fontSize};
  line-height: ${(props) => props.lineHeight};
  font-weight: ${(props) => props.fontWeight};
  letter-spacing: ${(props) => props.letterSpacing};

  @media (min-width: ${mediaQueries.min.large}) {
    font-size: ${(props) => props.desktopFontSize};
    line-height: ${(props) => props.desktopLineHeight};
  }
`;
H2Container.displayName = 'H2Container';

H2Container.defaultProps = {
  color: theme.headingTextColor,
  fontFamily: theme.headingFont,
};

const H3Container = styled.h3`
  color: ${(props) => props.color};
  font-family: ${(props) => props.fontFamily};
  font-size: ${(props) => props.fontSize};
  line-height: ${(props) => props.lineHeight};
  font-weight: ${(props) => props.fontWeight};
  letter-spacing: ${(props) => props.letterSpacing};

  @media (min-width: ${mediaQueries.min.large}) {
    font-size: ${(props) => props.desktopFontSize};
    line-height: ${(props) => props.desktopLineHeight};
  }
`;
H3Container.displayName = 'H3Container';

H3Container.defaultProps = {
  color: theme.headingTextColor,
  fontFamily: theme.headingFont,
};

const H4Container = styled.h4`
  color: ${(props) => props.color};
  font-family: ${(props) => props.fontFamily};
  font-size: ${(props) => props.fontSize};
  line-height: ${(props) => props.lineHeight};
  font-weight: ${(props) => props.fontWeight};
  letter-spacing: ${(props) => props.letterSpacing};

  @media (min-width: ${mediaQueries.min.large}) {
    font-size: ${(props) => props.desktopFontSize};
    line-height: ${(props) => props.desktopLineHeight};
  }
`;
H4Container.displayName = 'H4Container';

H4Container.defaultProps = {
  color: theme.headingTextColor,
  fontFamily: theme.headingFont,
};

const H5Container = styled.h5`
  color: ${(props) => props.color};
  font-family: ${(props) => props.fontFamily};
  font-size: ${(props) => props.fontSize};
  line-height: ${(props) => props.lineHeight};
  font-weight: ${(props) => props.fontWeight};
  letter-spacing: ${(props) => props.letterSpacing};

  @media (min-width: ${mediaQueries.min.large}) {
    font-size: ${(props) => props.desktopFontSize};
    line-height: ${(props) => props.desktopLineHeight};
  }
`;
H5Container.displayName = 'H5Container';

H5Container.defaultProps = {
  color: theme.headingTextColor,
  fontFamily: theme.headingFont,
};

const H6Container = styled.h6`
  color: ${(props) => props.color};
  font-family: ${(props) => props.fontFamily};
  font-size: ${(props) => props.fontSize};
  line-height: ${(props) => props.lineHeight};
  font-weight: ${(props) => props.fontWeight};
  letter-spacing: ${(props) => props.letterSpacing};

  @media (min-width: ${mediaQueries.min.large}) {
    font-size: ${(props) => props.desktopFontSize};
    line-height: ${(props) => props.desktopLineHeight};
  }
`;
H6Container.displayName = 'H6Container';

H6Container.defaultProps = {
  color: theme.bodyTextColor,
  fontFamily: theme.bodyFont,
  fontSize: theme.bodyFontSize,
};

const PContainer = styled.p`
  color: ${(props) => props.color};
  font-family: ${(props) => props.fontFamily};
  font-size: ${(props) => props.fontSize};
  line-height: ${(props) => props.lineHeight};
  font-weight: ${(props) => props.fontWeight};
  letter-spacing: ${(props) => props.letterSpacing};

  @media (min-width: ${mediaQueries.min.large}) {
    font-size: ${(props) => props.desktopFontSize};
    line-height: ${(props) => props.desktopLineHeight};
  }
`;
PContainer.displayName = 'PContainer';

PContainer.defaultProps = {
  fontFamily: theme.bodyFont,
};

const SpanContainer = styled.span`
  color: ${(props) => props.color};
  font-family: ${(props) => props.fontFamily};
  font-size: ${(props) => props.fontSize};
  line-height: ${(props) => props.lineHeight};
  font-weight: ${(props) => props.fontWeight};
  letter-spacing: ${(props) => props.letterSpacing};

  @media (min-width: ${mediaQueries.min.large}) {
    font-size: ${(props) => props.desktopFontSize};
    line-height: ${(props) => props.desktopLineHeight};
  }
`;
SpanContainer.displayName = 'SpanContainer';

SpanContainer.defaultProps = {
  fontFamily: theme.bodyFont,
};

const Label = ({
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  p,
  span,
  className,
  color,
  fontFamily,
  fontSize,
  desktopFontSize,
  fontWeight,
  letterSpacing,
  lineHeight,
  desktopLineHeight,
}) => {
  if (h1) {
    return (
      <H1Container
        className={className}
        color={color}
        fontFamily={fontFamily}
        fontSize={fontSize}
        desktopFontSize={desktopFontSize}
        fontWeight={fontWeight}
        letterSpacing={letterSpacing}
        lineHeight={lineHeight}
        desktopLineHeight={desktopLineHeight}
      >
        {h1}
      </H1Container>
    );
  }

  if (h2) {
    return (
      <H2Container
        className={className}
        color={color}
        fontFamily={fontFamily}
        fontSize={fontSize}
        desktopFontSize={desktopFontSize}
        fontWeight={fontWeight}
        letterSpacing={letterSpacing}
        lineHeight={lineHeight}
        desktopLineHeight={desktopLineHeight}
      >
        {h2}
      </H2Container>
    );
  }

  if (h3) {
    return (
      <H3Container
        className={className}
        color={color}
        fontFamily={fontFamily}
        fontSize={fontSize}
        desktopFontSize={desktopFontSize}
        fontWeight={fontWeight}
        letterSpacing={letterSpacing}
        lineHeight={lineHeight}
        desktopLineHeight={desktopLineHeight}
      >
        {h3}
      </H3Container>
    );
  }

  if (h4) {
    return (
      <H4Container
        className={className}
        color={color}
        fontFamily={fontFamily}
        fontSize={fontSize}
        desktopFontSize={desktopFontSize}
        fontWeight={fontWeight}
        letterSpacing={letterSpacing}
        lineHeight={lineHeight}
        desktopLineHeight={desktopLineHeight}
      >
        {h4}
      </H4Container>
    );
  }

  if (h5) {
    return (
      <H5Container
        className={className}
        color={color}
        fontFamily={fontFamily}
        fontSize={fontSize}
        desktopFontSize={desktopFontSize}
        fontWeight={fontWeight}
        letterSpacing={letterSpacing}
        lineHeight={lineHeight}
        desktopLineHeight={desktopLineHeight}
      >
        {h5}
      </H5Container>
    );
  }

  if (h6) {
    return (
      <H6Container
        className={className}
        color={color}
        fontFamily={fontFamily}
        fontSize={fontSize}
        desktopFontSize={desktopFontSize}
        fontWeight={fontWeight}
        letterSpacing={letterSpacing}
        lineHeight={lineHeight}
        desktopLineHeight={desktopLineHeight}
      >
        {h6}
      </H6Container>
    );
  }

  if (p) {
    return (
      <PContainer
        className={className}
        color={color}
        fontFamily={fontFamily}
        fontSize={fontSize}
        desktopFontSize={desktopFontSize}
        fontWeight={fontWeight}
        letterSpacing={letterSpacing}
        lineHeight={lineHeight}
        desktopLineHeight={desktopLineHeight}
      >
        {p}
      </PContainer>
    );
  }

  if (span) {
    return (
      <SpanContainer
        className={className}
        color={color}
        fontFamily={fontFamily}
        fontSize={fontSize}
        desktopFontSize={desktopFontSize}
        fontWeight={fontWeight}
        letterSpacing={letterSpacing}
        lineHeight={lineHeight}
        desktopLineHeight={desktopLineHeight}
      >
        {span}
      </SpanContainer>
    );
  }

  return null;
};

Label.propTypes = {
  h1: PropTypes.string,
  h2: PropTypes.string,
  h3: PropTypes.string,
  h4: PropTypes.string,
  h5: PropTypes.string,
  h6: PropTypes.string,
  p: PropTypes.string,
  span: PropTypes.string,
  className: PropTypes.string,
  color: PropTypes.string,
  fontFamily: PropTypes.string,
  fontSize: PropTypes.string,
  desktopFontSize: PropTypes.string,
  fontWeight: PropTypes.number,
  letterSpacing: PropTypes.string,
  lineHeight: PropTypes.string,
  desktopLineHeight: PropTypes.string,
};

export default Label;
