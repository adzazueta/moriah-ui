import React from 'react';
import PropTypes from 'prop-types';
import Navbar from './Navbar';
import Footer from './Footer';

const Layout = ({ children }) => {
  if (children) {
    return (
      <>
        <Navbar />
        {children}
        <Footer />
      </>
    );
  }

  return null;
};

Layout.propTypes = {
  children: PropTypes.element.isRequired,
};

export default Layout;
