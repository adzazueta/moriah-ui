import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import theme from '../assets/styles/theme';

const CardContainer = styled.div`
  background: url(${(props) => props.background});
  background-color: ${(props) => props.backgroundColor};
  background-size: ${(props) => props.backgroundSize};
  border-radius: ${theme.radius2x};
  box-shadow: ${theme.shadow1px};
`;
CardContainer.displayName = 'CardContainer';

// eslint-disable-next-line object-curly-newline
const Card = ({ children, url, backgroundColor, backgroundSize }) => (
  <CardContainer
    background={url}
    backgroundColor={backgroundColor}
    backgroundSize={backgroundSize}
  >
    {children}
  </CardContainer>
);

Card.propTypes = {
  children: PropTypes.element,
  url: PropTypes.string,
  backgroundColor: PropTypes.string,
  backgroundSize: PropTypes.string,
};

export default Card;
