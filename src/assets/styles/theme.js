export default {
  /**
   * @Font families
   */
  headingFont: 'Norwester',
  bodyFont: 'Avenir Next Condensed Medium',

  /**
   * @Font styles
   */
  logoFontSize: '24px',
  logoLetterSpacing: '2px',
  logoFontWeight: 400,
  bodyFontSize: '18px',
  smallFontSize: '14px',

  /**
   * @Font colors
   */
  headingTextColor: '#313131',
  bodyTextColor: '#969696',

  /**
   * @Background & contrast colors
   */
  blackColor: '#313131',
  mainColor: '#F4F4F4',
  moriahRedColor: '#FF3333',
  moriahAquamarineColor: '#EBFFFE',
  moriahGreencolor: '#00F88F',

  /**
   * @Sizes
   */
  iconSize: 48,
  radius1x: '4px',
  radius2x: '8px',
  shadow1px: '0px 3px 6px rgba(0, 0, 0, 0.2)',
};
