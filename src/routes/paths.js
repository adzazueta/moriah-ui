const HOME_PATH = '/';
const MENU_PATH = '/menu';
const SCHEDULE_PATH = '/schedule';

export default { HOME_PATH, MENU_PATH, SCHEDULE_PATH };
