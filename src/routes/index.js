import paths from './paths';
import pages from '../pages';

const HOME = {
  component: pages.Home,
  path: paths.HOME_PATH,
  isPrivate: false,
};

const MENU = {
  component: pages.Menu,
  path: paths.MENU_PATH,
  isPrivate: false,
};

const SCHEDULE = {
  component: pages.Schedule,
  path: paths.SCHEDULE_PATH,
  isPrivate: false,
};

export default [HOME, MENU, SCHEDULE];
