/* eslint-disable import/prefer-default-export */
const date = new Date();
const months = [
  'Enero',
  'Febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Septiembre',
  'Octubre',
  'Noviembre',
  'Diciembre',
];

const days = [
  'Domingo',
  'Lunes',
  'Martes',
  'Miercoles',
  'Jueves',
  'Viernes',
  'Sabado',
];

export const isOpen = () => {
  const hour = date.getHours();

  return hour >= 15 && hour < 23;
};

export const sortedDays = () => [...days.slice(1, 7), days[0]];

// eslint-disable-next-line prettier/prettier
export const getFormatedDate = () => `${days[date.getDay()]} ${date.getDate()} de ${months[date.getMonth()]} de ${date.getFullYear()}`;

export const getFormatedDay = () => `${days[date.getDay()]}`;
