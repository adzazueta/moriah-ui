import { createGlobalStyle } from 'styled-components';

import NorwesterFontWoff from '../assets/fonts/Norwester.woff';
import NorwesterFontWoff2 from '../assets/fonts/Norwester.woff2';
import AvenirNextCondenseWoff from '../assets/fonts/AvenirNextCondensedMedium.woff';
import AvenirNextCondensedWoff2 from '../assets/fonts/AvenirNextCondensedMedium.woff2';

export default createGlobalStyle`
  @font-face {
    font-family: 'Norwester';
    src: local('Norwester'), local('Norwester'),
    url(${NorwesterFontWoff2}) format('woff2'),
    url(${NorwesterFontWoff}) format('woff');
    font-weight: 300;
    font-style: normal;
  }

  @font-face {
    font-family: 'Avenir Next Condensed Medium';
    src: local('Avenir Next Condensed Medium'), local('Avenir'),
    url(${AvenirNextCondensedWoff2}) format('woff2'),
    url(${AvenirNextCondenseWoff}) format('woff');
    font-weight: 300;
    font-style: normal;
  }
`;
