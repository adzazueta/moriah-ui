import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Label from '../components/Label';
import Button from '../components/Button';

import theme from '../assets/styles/theme';
import MenuBackground from '../assets/icons/menu.png';

import { isOpen } from '../utils/date';

const ServicesSectionContainer = styled.div``;
ServicesSectionContainer.displayName = 'ServicesSectionContainer';

const Circle = styled.div`
  border-radius: 50%;
  background-color: ${(props) => props.color};
  width: 20px;
  height: 20px;
`;
Circle.displayName = 'Circle';

const ServicesSection = ({
  className,
  openMessage,
  closedMessage,
  open,
  menuTitle,
  scheduleTitle,
}) => (
  <ServicesSectionContainer className={className}>
    <div className="row">
      <div className="col-12 mb-2">
        <Label
          className="m-0 text-uppercase"
          h2="Utiliza nuestros servicios en linea"
        />
        <Label
          className="mt-1"
          h6="Queremos estar cerca de ti en el momento en el que lo necesites, al alcance de un click"
        />
      </div>
    </div>
    <div className="row">
      <div className="col-6">
        <Button
          src={MenuBackground}
          to="/menu"
          imageContainerClassName="col-9 py-2"
        >
          <div className="col-12">
            <Label className="text-uppercase pl-3 pt-3 m-0" h4={menuTitle} />
          </div>
        </Button>
      </div>
      <div className="col-6">
        <Button src={MenuBackground} to="/schedule" withoutImage>
          <div className="col-12">
            <Label
              className="text-uppercase m-0 pl-3 pt-3"
              h4={scheduleTitle}
            />
            <div className="row pt-5">
              <div className="col-4">
                <Circle
                  className="ml-3 mr-0 mt-1"
                  color={open ? theme.moriahGreencolor : theme.moriahRedColor}
                />
              </div>
              <div className="col-7 pl-0">
                <Label
                  className="text-uppercase mb-3"
                  fontFamily={theme.headingFont}
                  fontSize={theme.smallFontSize}
                  color={open ? theme.moriahGreencolor : theme.moriahRedColor}
                  p={open ? openMessage : closedMessage}
                />
              </div>
            </div>
          </div>
        </Button>
      </div>
    </div>
  </ServicesSectionContainer>
);

ServicesSection.defaultProps = {
  openMessage: 'Abierto hoy hasta las 11:00 P.M',
  closedMessage: 'Cerrado, abrimos de 3:00 P.M a 11:00 P.M',
  open: isOpen(),
  menuTitle: 'Menu',
  scheduleTitle: 'Horarios',
};

ServicesSection.propTypes = {
  className: PropTypes.string,
  openMessage: PropTypes.string,
  closedMessage: PropTypes.string,
  open: PropTypes.bool,
  menuTitle: PropTypes.string,
  scheduleTitle: PropTypes.string,
};

export default ServicesSection;
