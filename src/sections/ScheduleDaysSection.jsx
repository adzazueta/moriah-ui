import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import theme from '../assets/styles/theme';

import Label from '../components/Label';
import Card from '../components/Card';
import { sortedDays, isOpen, getFormatedDay } from '../utils/date';

const ScheduleDaysSectionContainer = styled.div``;
ScheduleDaysSectionContainer.displayName = 'ScheduleDaysSectionContainer';

const ScheduleDaysSection = ({ className }) => {
  const validateToday = (item) => {
    const date = new Date();
    const today = date.getDay();
    return item === today - 1;
  };

  const isOpenLabel = () => {
    if (getFormatedDay() === 'Lunes') {
      return 'Cerrado todo el día';
    }
    return isOpen()
      ? 'Abierto ahora hasta las 23:00hrs'
      : 'Abrimos de 3:00 P.M a 11:00 P.M';
  };

  return (
    <ScheduleDaysSectionContainer className={className}>
      {sortedDays().map((day, index) => (
        <div key={day} className="row mb-3 mt-2">
          <div className={validateToday(index) ? 'col-12' : 'col-10 offset-1'}>
            <Card
              backgroundColor={
                validateToday(index)
                  ? theme.moriahAquamarineColor
                  : theme.mainColor
              }
              className="mb-4"
            >
              <div className={validateToday(index) ? 'px-3 py-3' : 'px-3 py-2'}>
                <Label className="m-0 text-uppercase" h5={day} />
                <Label
                  className="m-0"
                  p={
                    validateToday(index)
                      ? isOpenLabel()
                      : index === 0
                        ? 'Cerrado todo el día'
                        : 'Abierto de 3:00 P.M a 11:00 P.M'
                  }
                  color={
                    validateToday(index)
                      ? index === 0
                        ? theme.moriahRedColor
                        : theme.moriahGreencolor
                      : theme.bodyTextColor
                  }
                />
              </div>
            </Card>
          </div>
        </div>
      ))}
    </ScheduleDaysSectionContainer>
  );
};

ScheduleDaysSection.propTypes = {
  className: PropTypes.string,
};

export default ScheduleDaysSection;
