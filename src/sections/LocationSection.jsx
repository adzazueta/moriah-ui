import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Label from '../components/Label';
import GoogleMap from '../components/GoogleMap';
import Button from '../components/Button';

import GoogleMapsImage from '../assets/icons/maps.png';
import WazeImage from '../assets/icons/waze.png';

const LocationSectionContainer = styled.div``;
LocationSectionContainer.displayName = 'LocationSectionContainer';

const LocationSection = ({ className }) => (
  <LocationSectionContainer className={className}>
    <div className="row">
      <div className="col-12 mb-2">
        <Label className="m-0 text-uppercase" h2="Ubicacion" />
        <Label
          className="mt-1"
          h6="Gaza 847, Miguel Hidalgo, Guadalajara Jal"
        />
      </div>
    </div>
    <div className="row">
      <div className="col-12 mb-2">
        <GoogleMap />
      </div>
    </div>
    <div className="row mt-2">
      <div className="col-3">
        <Button
          src={GoogleMapsImage}
          href="https://maps.app.goo.gl/6ghvCRQGMD67zkf26"
          imageButton
        />
      </div>
      <div className="col-3">
        <Button
          src={WazeImage}
          href="https://www.waze.com/ul?place=ChIJRRUoSK6xKIQRLeviTZwQt3c&ll=20.67463550%2C-103.28620840&navigate=yes"
          imageButton
        />
      </div>
    </div>
  </LocationSectionContainer>
);

LocationSection.propTypes = {
  className: PropTypes.string,
};

export default LocationSection;
