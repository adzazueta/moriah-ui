import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import theme from '../assets/styles/theme';

import Label from '../components/Label';
import ProductItem from '../components/ProductItem';
import { ExpandMoreIcon } from '../components/Icons';

const MenuListSectionContainer = styled.div``;
MenuListSectionContainer.displayName = 'MenuListSectionContainer';

const CategoryArrow = styled.div`
  float: right;
`;
CategoryArrow.displayName = 'CategoryArrow';

const CategoryCollapsible = styled.details`
  summary {
    outline: none;
  }

  .Product__Item {
    border-bottom: 2px ${theme.mainColor} solid;
  }

  > summary::-webkit-details-marker {
    display: none;
  }
`;
CategoryCollapsible.displayName = 'CategoryCollapsible';

const MenuListSection = ({ className }) => {
  const [menu, setMenu] = useState(false);

  useEffect(() => {
    fetch('https://api.moriah.com.mx/v1/dish')
      .then((res) => res.json())
      .then((data) => setMenu(data.dishes));
  }, []);

  const renderTypes = (type, dishes) => (
    <div className="col-12">
      <CategoryArrow className="Arrow__expand">
        <ExpandMoreIcon className="expand-arrow mr-2" />
      </CategoryArrow>
      <CategoryCollapsible>
        <summary className="pb-4 pl-0 col-12">
          <Label className="m-0 pl-1 text-uppercase" h4={type} />
        </summary>
        <div className="pb-4">
          {dishes.map((dish) => (
            <div key={dish._id} className="Product__Item py-2">
              <ProductItem
                productName={dish.name}
                productDescription={dish.description}
                productPrizes={dish.sizesAndPrices}
              />
            </div>
          ))}
        </div>
      </CategoryCollapsible>
    </div>
  );

  if (menu) {
    const entrees = menu.filter((dish) => dish.type === 'Entradas');
    const houseSpecialities = menu.filter(
      (dish) => dish.type === 'Especialidades de la casa',
    );
    const bagels = menu.filter((dish) => dish.type === 'Bagels');
    const pasta = menu.filter((dish) => dish.type === 'Pastas');
    const saltyCrepes = menu.filter((dish) => dish.type === 'Crepas Saladas');
    const sweetCrepes = menu.filter((dish) => dish.type === 'Crepas Dulces');
    const ciabattas = menu.filter((dish) => dish.type === 'Chapatas');
    const baguettes = menu.filter((dish) => dish.type === 'Baguettes');
    const sandwiches = menu.filter((dish) => dish.type === 'Sandwiches');
    const salads = menu.filter((dish) => dish.type === 'Ensaladas');
    const hotDrinks = menu.filter((dish) => dish.type === 'Bebidas Calientes');
    const coldDrinks = menu.filter((dish) => dish.type === 'Bebidas Frias');
    const specialDrinks = menu.filter(
      (dish) => dish.type === 'Bebidas Especiales',
    );
    const desserts = menu.filter((dish) => dish.type === 'Postres');
    const forChilds = menu.filter((dish) => dish.type === 'Para Peques');

    return (
      <MenuListSectionContainer className={className}>
        <div className="row">
          {renderTypes('Entradas', entrees)}
          {renderTypes('Especialidades de la casa', houseSpecialities)}
          {renderTypes('Bagels', bagels)}
          {renderTypes('Pastas', pasta)}
          {renderTypes('Crepas Saladas', saltyCrepes)}
          {renderTypes('Crepas Dulces', sweetCrepes)}
          {renderTypes('Chapatas', ciabattas)}
          {renderTypes('Baguettes', baguettes)}
          {renderTypes('Sandwiches', sandwiches)}
          {renderTypes('Ensaladas', salads)}
          {renderTypes('Bebidas Calientes', hotDrinks)}
          {renderTypes('Bebidas Frias', coldDrinks)}
          {renderTypes('Bebidas Especiales', specialDrinks)}
          {renderTypes('Postres', desserts)}
          {renderTypes('Para Peques', forChilds)}
        </div>
      </MenuListSectionContainer>
    );
  }

  return null;
};

MenuListSection.propTypes = {
  className: PropTypes.string,
};

export default MenuListSection;
