import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Label from '../components/Label';
import Button from '../components/Button';

import WhatsAppImage from '../assets/icons/whatsapp.png';
import FacebookImage from '../assets/icons/facebook.png';
import InstagramImage from '../assets/icons/instagram.png';
import TwitterImage from '../assets/icons/twitter.png';

const ContactSectionContainer = styled.div``;
ContactSectionContainer.displayName = 'ContactSectionContainer';

const ContactSection = ({ className }) => (
  <ContactSectionContainer className={className}>
    <div className="row">
      <div className="col-12 mb-2">
        <Label
          className="m-0 text-uppercase"
          h2="Ponte en contacto con nosotros"
        />
        <Label
          className="mt-1"
          h6="Sigenos en nuestras redes sociales, si necesitas algo mas especifico puedes escribirnos"
        />
      </div>
    </div>
    <div className="row">
      <div className="col-3">
        <Button
          src={WhatsAppImage}
          href="https://api.whatsapp.com/send?phone=525584909188"
          imageButton
        />
      </div>
      <div className="col-3">
        <Button
          src={FacebookImage}
          href="https://www.facebook.com/terrazamoriah"
          imageButton
        />
      </div>
      <div className="col-3">
        <Button
          src={InstagramImage}
          href="https://instagram.com/terrazamoriah?igshid=bnpm9i7o459i"
          imageButton
        />
      </div>
      <div className="col-3">
        <Button
          src={TwitterImage}
          href="https://twitter.com/terrazamoriah"
          imageButton
        />
      </div>
    </div>
  </ContactSectionContainer>
);

ContactSection.propTypes = {
  className: PropTypes.string,
};

export default ContactSection;
