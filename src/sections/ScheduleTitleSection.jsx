import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Label from '../components/Label';
import { getFormatedDate } from '../utils/date';

const ScheduleTitleSectionContainer = styled.div``;
ScheduleTitleSectionContainer.displayName = 'ScheduleTitleSectionContainer';

const ScheduleTitleSection = ({ className }) => (
  <ScheduleTitleSectionContainer className={className}>
    <Label className="text-uppercase" h1="Horarios" />
    <Label h6={getFormatedDate()} />
  </ScheduleTitleSectionContainer>
);

ScheduleTitleSection.propTypes = {
  className: PropTypes.string,
};

export default ScheduleTitleSection;
