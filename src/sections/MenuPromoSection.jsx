import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import theme from '../assets/styles/theme';

import ProductItem from '../components/ProductItem';
import Label from '../components/Label';

const MenuPromoSectionContainer = styled.div``;
MenuPromoSectionContainer.displayName = 'MenuPromoSectionContainer';

const MenuPromoSection = ({ className }) => (
  <MenuPromoSectionContainer className={className}>
    <div className="row">
      <div className="col-12">
        <Label
          className="m-0 text-uppercase"
          color={theme.moriahRedColor}
          h6="Promo de hoy"
        />
        <ProductItem
          productName="Pareja Hamburguesas Moriah"
          productDescription="Dos hamburguesas con pan horneado y costra de queso parmesano, carne especial de la casa a punto, jamon de pierna, queso gouda, jitomate saladel, lechuga fresca y aguacate"
          productPrizes={[{ size: '', price: 140 }]}
        />
      </div>
    </div>
  </MenuPromoSectionContainer>
);

MenuPromoSection.propTypes = {
  className: PropTypes.string,
};

export default MenuPromoSection;
