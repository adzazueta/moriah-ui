import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Label from '../components/Label';
import Button from '../components/Button';

import UberEatsImage from '../assets/icons/ubereats.png';
import DidiFoodsImage from '../assets/icons/didi.png';
import SinDelantalImage from '../assets/icons/sindelantal.png';

const DeliveryAppsSectionContainer = styled.div``;
DeliveryAppsSectionContainer.displayName = 'DeliveryAppsSectionContainer';

const DeliveryAppsSection = ({ className }) => (
  <DeliveryAppsSectionContainer className={className}>
    <div className="row">
      <div className="col-12 mb-2">
        <Label
          className="m-0 text-uppercase"
          h2="Ordena Moriah desde tus apps favoritas"
        />
        <Label
          className="mt-1"
          h6="Si ya eres usuario de alguna de estas plataformas, ve nuestro perfil y ordena"
        />
      </div>
    </div>
    <div className="row">
      <div className="col-3">
        <Button
          src={UberEatsImage}
          href="https://order.ubereats.com/guadalajara/food-delivery/Terraza%20Moriah/bfx5dt9yQAOlxw6pXWfXxg/?utm_source=web-restaurant-manager"
          rel="noopener noreferrer"
          alt="orderonline"
          imageButton
        />
      </div>
      <div className="col-3">
        <Button
          src={SinDelantalImage}
          href="https://www.sindelantal.mx/delivery/guadalajara-jal/terraza-moriah-miguel-hidalgo/1c779280-91b7-4460-bd0b-7c2fd833aabc"
          imageButton
        />
      </div>
      <div className="col-3">
        <Button
          src={DidiFoodsImage}
          href="https://www.didi-food.com/es-MX"
          imageButton
        />
      </div>
    </div>
  </DeliveryAppsSectionContainer>
);

DeliveryAppsSection.propTypes = {
  className: PropTypes.string,
};

export default DeliveryAppsSection;
