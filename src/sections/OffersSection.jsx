import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import CarouselItem from '../components/CarouselItem';
import Slideshow from '../components/Slideshow';

const OffersSectionContainer = styled.div``;
OffersSectionContainer.displayName = 'OffersSectionContainer';

const OffersSection = ({ className }) => (
  <OffersSectionContainer className={className}>
    <Slideshow>
      <CarouselItem
        offerType="Promocion"
        offerTitle="pareja hamburguesas"
        offerDescription="Dos Hamburguesas Moriah por $140"
        offerIcon="lel"
        src="https://static-images.ifood.com.br/image/upload/f_auto,t_medium/pratos/1c779280-91b7-4460-bd0b-7c2fd833aabc/202004171929_ynLU_h.jpg"
      />
      <CarouselItem
        offerType="Aviso"
        offerTitle="reseva en exterior"
        offerDescription="Escríbenos para agendar tu reservacion"
        offerIcon="lel"
        src="https://scontent.fgdl3-1.fna.fbcdn.net/v/t1.0-9/114514132_2631485070399286_2725137199894120391_o.jpg?_nc_cat=104&_nc_sid=730e14&_nc_eui2=AeEm7mgi1C0WaQM4TCwB7nF0MjXQAwceQe0yNdADBx5B7R6ttW7L5vQXPh6c7xVbbtXS-wqNkQD1mu5-0j-qVGb8&_nc_ohc=xRXqPCEQq1QAX_si5wA&_nc_ht=scontent.fgdl3-1.fna&oh=efc76a9254d6162af20c45afcb8bdf6d&oe=5F55082F"
      />
    </Slideshow>
  </OffersSectionContainer>
);

OffersSection.propTypes = {
  className: PropTypes.string,
};

export default OffersSection;
