import React, { Suspense } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Head from './components/Head';
import GlobalFonts from './utils/fonts';
import Routes from './routes';
import Layout from './components/Layout';

const App = () => (
  <>
    <Head />
    <GlobalFonts />
    <Router>
      <Layout>
        <Suspense fallback="Loading...">
          <Switch>
            {Routes.map((route) => (
              <Route
                key={route.path}
                path={route.path}
                component={route.component}
                isPrivate={route.isPrivate}
                exact
              />
            ))}
          </Switch>
        </Suspense>
      </Layout>
    </Router>
  </>
);

export default App;
