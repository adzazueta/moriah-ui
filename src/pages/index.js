import { lazy } from 'react';

const Home = lazy(() => import('./Home'));
const Menu = lazy(() => import('./Menu'));
const Schedule = lazy(() => import('./Schedule'));

export default {
  Home,
  Menu,
  Schedule,
};
