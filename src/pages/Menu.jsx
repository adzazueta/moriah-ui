import React from 'react';
import styled from 'styled-components';

import theme from '../assets/styles/theme';

import Label from '../components/Label';
import MenuPromoSection from '../sections/MenuPromoSection';
import MenuListSection from '../sections/MenuListSection';

const MenuContainer = styled.div``;
MenuContainer.displayName = 'MenuContainer';

const Menu = () => (
  <MenuContainer className="container">
    <div className="row">
      <div className="col-12 col-lg-6 offset-lg-3">
        <Label className="m-0 text-uppercase" h1="Menu" />
      </div>
      <MenuPromoSection className="col-12 col-lg-6 offset-lg-3 mt-3" />
      <MenuListSection className="col-12 col-lg-6 offset-lg-3 mt-4" />
      <div className="col-12 text-center pt-4">
        <Label
          className="mt-5"
          span="Todos nuestros precios están expresados en MXN"
          color={theme.bodyTextColor}
          fontFamily={theme.bodyFont}
        />
      </div>
    </div>
  </MenuContainer>
);

export default Menu;
