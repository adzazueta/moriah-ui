import React from 'react';
import styled from 'styled-components';

import OffersSection from '../sections/OffersSection';
import ServicesSection from '../sections/ServicesSection';
import DeliveryAppsSection from '../sections/DeliveryAppsSection';
import LocationSection from '../sections/LocationSection';
import ContactSection from '../sections/ContactSection';

const HomeContainer = styled.div``;
HomeContainer.displayName = 'HomeContainer';

const Home = () => (
  <HomeContainer className="container pb-4">
    <div className="row">
      <OffersSection className="col-12 col-lg-6 offset-lg-3 p-0" />
      <ServicesSection className="col-12 col-lg-6 offset-lg-3 mt-4 pb-3" />
      <DeliveryAppsSection className="col-12 col-lg-6 offset-lg-3 mt-4 pb-3" />
      <LocationSection className="col-12 col-lg-6 offset-lg-3 mt-4 pb-3" />
      <ContactSection className="col-12 col-lg-6 offset-lg-3 mt-4 pb-3" />
    </div>
  </HomeContainer>
);

export default Home;
