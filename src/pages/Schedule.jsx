import React from 'react';
import styled from 'styled-components';

import ScheduleTitleSection from '../sections/ScheduleTitleSection';
import ScheduleDaysSection from '../sections/ScheduleDaysSection';

const ScheduleContainer = styled.div``;
ScheduleContainer.displayName = 'ScheduleContainer';

const Schedule = () => (
  <ScheduleContainer className="container pb-4">
    <div className="row">
      <ScheduleTitleSection className="col-12 col-lg-6 offset-lg-3 pb-2" />
      <ScheduleDaysSection className="col-12 col-lg-6 offset-lg-3" />
    </div>
  </ScheduleContainer>
);

export default Schedule;
